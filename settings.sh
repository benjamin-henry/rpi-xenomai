#!/bin/bash

export LIBS_XC="build-essential libtool qt5-default autoconf qt4-dev-tools libqt4-dev pkg-config git bison flex libssl-dev make libc6-dev libncurses5-dev ncurses-dev bc"

#dossiers d'installation
export ROOTDIR=.
export MAIN_DIR=kernel_xeno #changer le .gitignore si modifié
export KNL_DIR=rt-kernel
export INSTALL_MOD_PATH=$ROOTDIR/$MAIN_DIR/$KNL_DIR
export INSTALL_DTBS_PATH=$ROOTDIR/$MAIN_DIR/$KNL_DIR

#arch pc compilateur
export BUILD=i686-pc-linux-gnu
export XENO_CORE=cobalt

#arch cible
export ARCH=arm

#compilateur
export CROSS_COMPILE=arm-linux-gnueabihf-

#ajout du compilateur pour rpi0 et 1 dans le path
export PI0_PATH=$PATH:$ROOTDIR/$MAIN_DIR/tools/arm-bcm2708/arm-linux-gnueabihf/bin