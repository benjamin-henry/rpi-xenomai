# RPi Xenomai

A shell script (and maybe more) to install Xenomai 3 on your RPi.

## Utilisation du scipt

Téléchargement des scripts :

```shell
git clone https://gitlab.com/benjamin-henry/rpi-xenomai.git
```

Modification des variables d'environnement (`settings.sh`) :

```shell
nano setting.sh
```

Lancement du script :

```shell
./install.sh
```

----

## WSL (Sous-système Windows pour Linux)

[Installation](https://link)

Il suffit ensuite de lancer le sous-système installé.

Il est obligatoire d'[installer un X-Server Windows](https://sourceforge.net/projects/vcxsrv/) pour l'affichage graphique du sous-système.

[Commandes WSL sous Windows](https://devblogs.microsoft.com/commandline/a-guide-to-invoking-wsl/)

Ajout d'alias utiles :

```shell
sudo nano /etc/bash.aliases
```

Ajouter dans `/etc/bash.aliases` (remplacer `USER_NAME` par l'utilisateur Windows) :

Pour WSL,

```shell
##Aliases for all
#ls aliases
alias ll='ls -al'
alias la='ls -A'
alias l='ls -CF'
#windows aliases
alias xservstart='/mnt/c/Logiciels/VcXsrv/vcxsrv.exe'
alias xservsettings='/mnt/c/Logiciels/VcXsrv/xlaunch.exe'
#windows functions
winexp(){ #ouverture du dossier courant dans un explorateur Windows
        explorer.exe $(wslpath -w '$1');
}
vscode(){ #ouverture d'un fichier avec vscode
        /mnt/c/Users/USER_NAME/AppData/Local/Programs/'Microsoft VS Code'/Code.exe $(wslpath -w '$1');
}
#other aliases
alias configssh='sudo nano /etc/ssh/ssh_config'"
```

Pour WSL 2,

```shell
##Aliases for all
#ls aliases
alias ll='ls -al'
alias la='ls -A'
alias l='ls -CF'
#windows aliases
alias xservstart='/mnt/c/Logiciels/VcXsrv/vcxsrv.exe'
alias xservsettings='/mnt/c/Logiciels/VcXsrv/xlaunch.exe'
#windows functions
winexp(){ #ouverture du dossier courant ou spécifié dans un explorateur Windows
        explorer.exe $1;
}
vscode(){ #ouverture d'un fichier avec vscode
        /mnt/c/Users/USER_NAME/AppData/Local/Programs/'Microsoft VS Code'/Code.exe $1;
}
#other aliases
alias configssh='sudo nano /etc/ssh/ssh_config'"
```

Ajouter dans `/etc/bash.bashrc` :

```shell
#include alias file
if [ -f /etc/bash.aliases ]; then
    . /etc/bash.aliases
fi
```

----

## Installation de Raspbian (en Handless)

[Lien](https://desertbot.io/blog/headless-pi-zero-w-wifi-setup-windows)

Putty n'est pas forcément nécessaire, Windows 10 inclue OpenSSH. Vous pouvez donc utiliser `ssh user@address` dans un invité de commande Windows.
Vous pouvez aussi utiliser WSL ou un OS Linux (sous VM par exemple). Une de ces deux installations sera nécessaire pour la cross-compilation.

----

## Ressources web

[Page officielle de Raspberry sur la compilation de leur noyau Raspbian](https://www.raspberrypi.org/documentation/linux/kernel)

[GCC on ARM](https://gist.github.com/fm4dd/c663217935dc17f0fc73c9c81b0aa845)

----

## Notes

### Config kernel conseillée

.config :

- Remove
  - CPU Power Management > CPU Frequency scaling > CPU Frequency scaling
  - Kernel Features > Contiguous Memory Allocator
  - Kernel Features > Allow for memory compaction
  - General setup > Profiling support
  - Kernel hacking > Tracers
  - Kernel hacking > KGDB-Kernel hacking
- Add
  - Xenomai/Cobalt > Drivers > Real-time IPC drivers > RTIPC protocol family
  - Xenomai/Cobalt > Drivers > Real-time GPIO drivers
  - Xenomai/Cobalt > Drivers > Real-time SPI masters drivers

### Dossiers Xenomai sous RPi

- /sys/module/xenomai
- /proc/xenomai
- /usr/xenomai
- /usr/xenomai/lib/xenomai
- /usr/xenomai/include/xenomai
- /lib/modules/4.14.37-ipipe+/kernel/drivers/xenomai

### Modules management

Utilisation de rmmod et insmod (remove et install modules) :

```shell
insmod mod_path  #ex: /lib/modules/4.14.37-ipipe+/kernel/drivers/xenomai/ipc/xeno_rtipc.ko
```

Utilisation de modprob :

```shell
modprobe -a nom_module    #add
modprobe -r nom_module    #rm
sudo modprobe -a xeno_spi xeno_rtipc # example
```

### Développement en C sous Linux Xenomai

Voir dossier dev_C_xenomai
