#!/bin/bash

# TODO: test variables contenues dans install.save

ask_phase() {
    if [[ $PAUSE == "y" ]]
    then 
        echo "> Voulez-vous passer cette étape ? (y/N)" && read CHOIX
        if [[ $CHOIX != "y" ]]
        then
            $FCT
        fi
    else
        $FCT
    fi
}

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Préparation de l'installation : 
définition des variables d'installation
(passer si variables déjà définis)"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

echo "> Voulez-vous passer cette étape ? (y/N)" && read CHOIX
if [[ $CHOIX != "y" ]]
then
    echo "> Activer les pauses régulières (choix des étapes à faire) ? (y/N)"
    read PAUSE

    while [[ $RPI != "1" ]] && [[ $RPI != "0" ]] && [[ $RPI != "2" ]]
    do
        echo "> Taper : 
            [0] pour RPi 0 ou 1
            [1] pour RPi 2 ou 3
            [2] pour RPi 4
        "
        read RPI
    done

    ok=0
    while [[ $ok != "y" ]]
    do
        echo "> Nombre de coeur pour la compilation :"
        read CORE
        
        echo "> Confirmer l'utilisation de '$CORE' coeur(s) (y/N)"
        read ok
    done

    echo "> Voulez-vous générer un nouveau fichier de configuration pour la compilation du noyeau ? (y/N)"
    read CHOICE
    if [[ $CHOICE == "y" ]]
    then
        echo "> Utiliser un menu de configuration graphique ? (y/N)"
        read GRAPH
    else
        ok=0
        while [[ $ok != "y" ]]
        do
            echo "> Indiquer le chemin absolu du fichier '.config' (ex: /home/.config_perso) :"
            read PATH_CONF
            
            echo "> Confirmer '$PATH_CONF' (y/N)"
            read ok
        done
    fi

    ok=0
    while [[ $ok != "y" ]]
    do
        echo "> Adresse de la RPi :"
        read ADDR
        
        echo "> Confirmer l'adresse '$ADDR' (y/N)"
        read ok
    done

    echo "> Utilisateur RPi :"
    read RPI_USR

    echo "> Mdp RPi :"
    read MDP

    echo "> Sauvegarde des variables dans install.save"
    echo -e "export PAUSE='$PAUSE'\nexport RPI='$RPI'\nexport CORE='$CORE'\nexport GRAPH='$GRAPH'\nexport PATH_CONF='$PATH_CONF'\nexport ADDR='$ADDR'\nexport RPI_USR='$RPI_USR'\nexport MDP='$MDP'" > install.save
    chmod 770 install.save

else
    if [ -f install.save ]; then . ./install.save
    else
        echo "> Veuillez définir les variables d'installations (au moins une fois)"
        exit 1
    fi
fi

chmod 770 settings.sh
. ./settings.sh
export KERNEL=$([ $RPI == "0" ] && echo "kernel" || ([ $RPI == "1" ] && echo "kernel7" || echo "kernel171"))
export BCM_CONF=$([ $RPI == "0" ] && echo "bcmrpi_defconfig" || ([ $RPI == "1" ] && echo "bcm2709_defconfig" || echo "bcm2711_defconfig"))
export CFLAGS=$([ $RPI == "0" ] && echo "-march=armv6 -mfpu=vfp -mfloat-abi=hard -marm" || ([ $RPI == "1" ] && echo "-march=armv7-a -mfpu=vfp3" || echo "-march=armv6-a -mfpu=vfp4"))
export LDFLAGS=$([ $RPI == "0" ] && echo "-mtune=arm1176jzf-s" || ([ $RPI == "1" ] && echo "-mtune=cortex-a53" || echo "-mtune=cortex-a72"))
if [ $RPI == "0" ]; then export PATH=$PI0_PATH; fi
if [ ! -d "$ROOTDIR/$MAIN_DIR/$KNL_DIR" ]; then mkdir -p $ROOTDIR/$MAIN_DIR/$KNL_DIR; fi

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Tests et setup de l'environnement"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

echo "> Sudo login :"
sudo uname -a

echo -e "\n> Trying SSH connection to RPi..."
if sshpass -p "$MDP" ssh -t $RPI_USR@$ADDR StrictHostKeyChecking=accept-new "sudo uname -a"
then
    echo "> OK"
else
    echo -e "> Cannot connect to RPi\n> End\n"
    exit 1
fi

cd $ROOTDIR/$MAIN_DIR

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Téléchargement des ressources linux"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

res_linux() {

    echo -e "> Ce script requiert les packets suivants (installés par la suite) :\n$LIBS_XC\n> Entrer pour commencer...\n"
    read

    if [ -v LIBS_XC ]; then sudo apt update && sudo apt install -y $LIBS_XC; fi

    if [ $RPI == "2" ]
    then
        git clone -b rpi-4.19.y https://github.com/raspberrypi/linux.git
        git reset --hard c078c64fecb325ee86da705b91ed286c90aae3f6
    else
        git clone -b rpi-4.14.y https://github.com/raspberrypi/linux.git
        git reset --hard 29653ef5475124316b9284adb6cbfc97e9cae48f
    fi

    if [ $RPI == "0" ]
    then
        sudo apt remove -y gcc-arm-linux-gnueabihf
        git clone https://github.com/raspberrypi/tools
    else
        sudo apt install -y gcc-arm-linux-gnueabihf
    fi
}
FCT=res_linux
ask_phase

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Obtention des codes sources"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

dl_src() {
    if [ $RPI = "2" ]
    then
        wget https://xenomai.org/downloads/xenomai/stable/xenomai-3.0.9.tar.bz2
        tar xjf xenomai-3.0.9.tar.bz2
        git clone https://github.com/thanhtam-h/rpi4-xeno3.git
    else
        wget https://xenomai.org/downloads/ipipe/v4.x/arm/ipipe-core-4.14.36-arm-1.patch
        wget http://xenomai.org/downloads/xenomai/stable/xenomai-3.0.7.tar.bz2
        tar xjf xenomai-3.0.7.tar.bz2
        cd linux
        cd ..
        wget https://raw.githubusercontent.com/lemariva/RT-Tools-RPi/master/xenomai/v3.0.7/irq-bcm2835.c
        wget https://raw.githubusercontent.com/lemariva/RT-Tools-RPi/master/xenomai/v3.0.7/irq-bcm2836.c
        cp irq-bcm2835.c linux/drivers/irqchip/
        cp irq-bcm2836.c linux/drivers/irqchip/
    fi
}
FCT=dl_src
ask_phase

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Patching du noyau"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

patching() {
    if [ $RPI = "2" ]
    then
        cd linux
        patch -p1 < ../rpi4-xeno3/scripts/pre-rpi4-4.19.86-xenomai3-simplerobot.patch
        cd ..
        xenomai-3.0.9/scripts/prepare-kernel.sh --linux=linux/ --arch=$ARCH --ipipe=../rpi4-xeno3/scripts/ipipe-core-4.19.82-arm-6-mod-4.49.86.patch
    else
        xenomai-3.0.7/scripts/prepare-kernel.sh --linux=linux/ --arch=$ARCH --ipipe=ipipe-core-4.14.36-arm-1.patch --verbose
    fi
}
FCT=patching
ask_phase

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Configuration du noyau"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

knl_config() {
    cd $ROOTDIR/$MAIN_DIR/linux/
    make $BCM_CONF
    if [ -v PATH_CONF ]
    then
        cp $PATH_CONF $INSTALL_MOD_PATH/.config
    else
        [[ $GRAPH == "y" ]] && make xconfig || make menuconfig
    fi
}
FCT=knl_config
ask_phase

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Compilation du noyau"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

knl_compl() {
    make -j$CORE zImage
    make -j$CORE modules
    make -j$CORE dtbs
    make -j$CORE modules_install
    make -j$CORE dtbs_install
    mkdir $INSTALL_MOD_PATH/boot
    ./scripts/mkknlimg ./arch/arm/boot/zImage $INSTALL_MOD_PATH/boot/$KERNEL.img

    if [ $RPI = "2" ]
    then
        echo "**Disable DWC features which may cause ipipe issue**
        dwc_otg.fiq_enable=0 dwc_otg.fiq_fsm_enable=0 dwc_otg.nak_holdoff=0

        **CPU affinity**
        isolcpus=0,1 xenomai.supported_cpus=0x3" > $INSTALL_MOD_PATH/boot/cmdline.txt
    fi
}
FCT=knl_compl
ask_phase
 
echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Transfert du noyau vers le Raspberry"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

knl_to_rpi() {
    cd $INSTALL_MOD_PATH
    tar czf ../xenomai-kernel.tgz *
    cd ..
    sshpass -p "$MDP" scp xenomai-kernel.tgz $RPI_USR@$ADDR:/tmp
}
FCT=knl_to_rpi
ask_phase

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Mise en place du noyau sur le Raspberry à
l'adresse $ADDR"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

knl_on_rpi() {
    sshpass -p "$MDP" ssh -t $RPI_USR@$ADDR "
    cd /tmp
    tar xzf xenomai-kernel.tgz
    sudo cp *.dtb /boot/
    cd boot
    sudo cp -rd * /boot/
    cd ../lib
    sudo cp -dr * /lib/
    cd ../overlays
    sudo cp -d * /boot/overlays
    cd ..
    sudo cp -d bcm* /boot/
    sudo reboot"
}
FCT=knl_on_rpi
ask_phase

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Vérification"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

sleep 240 #attente du redémarrage de la pi
verif() {
    if sshpass -p "$MDP" ssh -t $RPI_USR@$ADDR "
    uname -a
    cat /proc/xenomai/version
    cat /proc/ipipe/version"
    then
        echo "> Installation OK"
    else
        echo "> Problème lors de la reconnexion à la RPi... Erreur d'installation de Xenomai ?"
        exit 1
    fi
}
FCT=verif
ask_phase

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Compilation outils Xenomai"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

xeno_tools_compl() {
    cd $ROOTDIR/$MAIN_DIR/xenomai-3.0.7
    ./scripts/bootstrap --with-core=$XENO_CORE --enable-debug=partial
    ./configure CFLAGS=$CFLAGS LDFLAGS=$LDFLAGS --build=$BUILD --host=arm-linux-gnueabihf --with-core=$XENO_CORE --enable-smp --enable-dlopen-libs CC=${CROSS_COMPILE}gcc LD=${CROSS_COMPILE}ld
    make -j$CORE install DESTDIR=${PWD}/target
}
FCT=xeno_tools_compl
ask_phase

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Transfert outils vers RPi"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

tools_to_rpi() {
    cd $ROOTDIR/$MAIN_DIR/xenomai-3.0.7/target
    tar czf ../../xenomai-tools.tgz *
    cd $ROOTDIR/$MAIN_DIR
    sshpass -p "$MDP" scp xenomai-tools.tgz $RPI_USR@$ADDR:/tmp
}
FCT=tools_to_rpi
ask_phase

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Mise en place sur RPi"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

inst_on_rpi() {
    sshpass -p "$MDP" ssh -t $RPI_USR@$ADDR"
    cd /tmp
    sudo tar xzf xenomai-tools.tgz
    sudo mv dev/* /dev/
    sudo cp -r usr/* /usr/"
}
FCT=inst_on_rpi
ask_phase

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Test"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

test_tools() {
    sshpass -p "$MDP" ssh -t $RPI_USR@$ADDR ${
    sudo /usr/xenomai/bin/latency
    sudo su
    echo "

    export XENOMAI_ROOT_DIR=/usr/xenomai
    export XENOMAI_PATH=/usr/xenomai
    export PATH=$PATH:$XENOMAI_PATH/bin:$XENOMAI_PATH/sbin
    export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$XENOMAI_PATH/lib/pkgconfig
    if [ -v LD_LIBRARY_PATH ]
    then
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$XENOMAI_PATH/lib
    else
        export LD_LIBRARY_PATH=$XENOMAI_PATH/lib
    fi
    " >> /etc/bash.bashrc
    source /etc/bash.bashrc}
}
FCT=test_tools
ask_phase

# /!\ RPi 0,1,2 et 3 seulement /!\ 
echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Préparation RPi pour compilation de programmes
Xenomai en local"
echo "||||||||||||||||||||||||||||||||||||||||||||||||"

mod_compl_prep() {
    if [ $RPI = "2" ]
    then
    else
        cd $ROOTDIR/$MAIN_DIR
        tar -czf linux.tgz linux/*
        tar -czf rt-tools.tgz ipipe-core-4.14.36-arm-1.patch irq-bcm283*
        sshpass -p "$MDP" scp linux.tgz $RPI_USR@$ADDR:/tmp
        sshpass -p "$MDP" scp xenomai-3.0.7.tar.bz2 $RPI_USR@$ADDR:/tmp
        sshpass -p "$MDP" scp rt-tools.tgz $RPI_USR@$ADDR:/tmp
        sshpass -p "$MDP" ssh -t $RPI_USR@$ADDR"
        cd /tmp
        sudo mv xenomai-3.0.7.tar.bz2 /usr/src/
        cd /usr/src
        tar -xjf xenomai-3.0.7.tar.bz2
        sudo mv *.tgz /usr/src/
        tar -xzf *.tgz
        xenomai-3.0.7/scripts/prepare-kernel.sh --linux=linux/ --arch=$ARCH --ipipe=ipipe-core-4.14.36-arm-1.patch --verbose"
        sshpass -p "$MDP" scp $ROOTDIR/$MAIN_DIR/linux/.config $RPI_USR@$ADDR:/tmp
        sshpass -p "$MDP" ssh -t $RPI_USR@$ADDR"
        cd /tmp
        sudo mv .config /usr/src/linux/
        cd linux
        sudo apt install -y bc
        make prepare
        make scripts"
    fi
}
FCT=mod_compl_prep
ask_phase

echo "||||||||||||||||||||||||||||||||||||||||||||||||"
echo "> Fin de l'installation !"