# Bases pour développement de modules Xenomai

- [Bases pour développement de modules Xenomai](#bases-pour-d%c3%a9veloppement-de-modules-xenomai)
  - [Prérequis](#pr%c3%a9requis)
    - [Variable path des libs Xenomai](#variable-path-des-libs-xenomai)
    - [Vérouillage des pages mémoires](#v%c3%a9rouillage-des-pages-m%c3%a9moires)
    - [Import de librairies](#import-de-librairies)
  - [Fonctions utilitaires](#fonctions-utilitaires)
  - [Création de tâches Xenomai](#cr%c3%a9ation-de-t%c3%a2ches-xenomai)
    - [Explication des paramètres](#explication-des-param%c3%a8tres)
    - [Exemple](#exemple)
  - [Erreurs](#erreurs)
  - [Compilation](#compilation)
    - [Exemple de Makefile](#exemple-de-makefile)
  - [Thread](#thread)
    - [Check du fonctionnement des threads Xenomai](#check-du-fonctionnement-des-threads-xenomai)
    - [Passer un thread Utilisateur en Temps-réel](#passer-un-thread-utilisateur-en-temps-r%c3%a9el)
  - [Tâches périodiques](#t%c3%a2ches-p%c3%a9riodiques)
    - [RT_TASK tache](#rttask-tache)
    - [Unités de temps](#unit%c3%a9s-de-temps)
    - [RTIME start](#rtime-start)
    - [RTIME period](#rtime-period)
    - [Exemple](#exemple-1)
  - [Dohell](#dohell)
  - [Sémaphore](#s%c3%a9maphore)
    - [Fonctions usuelles](#fonctions-usuelles)
    - [Explication des paramètres](#explication-des-param%c3%a8tres-1)
    - [Exemple (exemple-semaphore.c)](#exemple-exemple-semaphorec)
  - [Mutex](#mutex)
    - [Fonctions usuelles](#fonctions-usuelles-1)
    - [Exemple d'utilisation](#exemple-dutilisation)
  - [Pipes](#pipes)
    - [Introduction](#introduction)
    - [Fonctions usuelles](#fonctions-usuelles-2)
    - [Exemple Pipe côté Linux](#exemple-pipe-c%c3%b4t%c3%a9-linux)
    - [Exemple Pipe côté Xenomai](#exemple-pipe-c%c3%b4t%c3%a9-xenomai)
  - [Annexes](#annexes)
    - [Xenomai](#xenomai)
    - [Make](#make)

----

## Prérequis

### Variable path des libs Xenomai

```shell
export LD_LIBRARY_PATH=/usr/xenomai/lib
```

Il est conseillé de l'ajouter à la fin de `~/.bashrc` (pour l'utilisateur courant) ou bien à la fin de `/etc/bash.bashrc`.

### Vérouillage des pages mémoires

L’utilisation de la fonction mlockall() permet de verrouiller en mémoire physique les pages utilisées par le processus, évitant ainsi tous les comportements imprévisibles liés à la gestion de la mémoire virtuelle. Avec Xenomai, cet appel est indispensable, car le déclenchement d’une faute de page (lors d’un accès à une zone qui n’est pas encore disponible) entraînerait un retour en mode secondaire. Ceci se produirait de manière totalement imprévisible puisque par défaut, c’est à l’instant de la première utilisation d’une page mémoire qu’elle est attribuée par le noyau, et non lors de son allocation initiale.
Tout programme Xenomai devra donc commencer – avant de lancer ses tâches temps réel – par une invocation :

```c
mlockall(MCL_CURRENT | MCL_FUTURE);
```

### Import de librairies

*Fichiers Rôles*

- <alchemy/alarm.h> Programmation d’alarme pour activer un thread temps réel de manière différée et/ou périodique.
- <alchemy/buffer.h> Communication Fifo par blocs mémoire entre threads ou processus.
- <alchemy/cond.h> Synchronisation sur des variables conditions proches des pthread_cond_t.
- <alchemy/event.h> Notification d’événements en utilisant des flags programmables.
- <alchemy/heap.h> Allocation de mémoire par tas, éventuellement partagés entre processus.
- <alchemy/intr.h> Gestion des interruptions depuis l’espace utilisateur.
- <alchemy/misc.h> Autorisation d’accès aux ports d’entrées-sorties.
- <alchemy/mutex.h> Synchronisation sur des mutex proches des pthread_mutex_t.
- <alchemy/pipe.h> Communication par tubes nommés avec les processus de l’espace Linux.
- <alchemy/queue.h> Transmission de messages sans copie (buffers partagés).
- <alchemy/sem.h> Synchronisation par sémaphores.
- <alchemy/task.h> Fonctions et types permettant de gérer les tâches : création, configuration, destruction, mise en sommeil, envoi de message, etc.
- <alchemy/timer.h> Configuration, lecture et conversion de l’heure système.

----

## Fonctions utilitaires

Fonctions utilitaires à utiliser à la place des fonctions C

```c
// bibliothèque RTDK (intégrée dans Xenomai) :
int rt_printf (const char * format...);
int rt_vprintf (const char * format, va_list arguments);
int rt_fprintf (FILE * stream, const char * format...);
int rt_vfprintf (FILE * stream, const char * format, va_list arguments);
int rt_puchar(int c);
int rt_fputc(int c, FILE *stream);
int rt_puts (const char * string);
int rt_fputs(const char *string, FILE *stream);
int rt_fwrite(const void *buffer, size_t size, size_t nb, FILE *stream);
void rt_syslog (int priority, const char *format...);
void rt_vsyslog (int priority, const char *format, va_list args);
```

----

## Création de tâches Xenomai

La création d’une tâche peut être réalisée avec rt_task_create(), qui prépare toutes les structures de données nécessaires au fonctionnement d’un nouveau thread, suivi de rt_task_start(), qui démarre véritablement le thread suspendu. On peut également regrouper les deux opérations en une seule avec rt_task_spawn(), qui crée une tâche et la laisse démarrer immédiatement.

```c
int rt_task_create (RT_TASK * task,
    int stack_size,
    const char * name,
    int priority,
    int mode);
int rt_task_start (RT_TASK * task,
    void (* function) (void *arg),
    void * arg);
int rt_task_spawn (RT_TASK * task, const char * name,
    int stack_size,
    int priority,
    int mode,
    void (* function) (void * arg),
    void * arg);
```

### Explication des paramètres

```c
task :
    Pointeur sur une structure qui sera initialisée durant l’appel
    à rt_task_create() pour représenter la tâche temps réel.
name :
    Nom de la tâche tel qu’il apparaît dans /proc/xenomai/sched.
stack_size :
    Taille de la pile. Si la valeur est nulle, une taille par défaut
    est affectée permettant de stocker 1 024 entiers.
priority :
    La priorité va de 1 à 99 dans l’espace des tâches Xenomai
    de manière similaire aux threads temps réel souple de Linux.
mode :
    Configuration du thread temps réel.
function :
    La fonction que le thread doit exécuter.
arg :
    Argument que la fonction du thread temps réel reçoit en paramètre
    à son démarrage.
T_CPU(numero) :
    La tâche s’exécutera sur le CPU indiqué. Le numero
    doit être strictement inférieur à RTHAL_NR_CPUS.
T_FPU :
    La tâche temps réel utilisera les ressources de la FPU
    (Floating Point Unit). Il s’agit d’un attribut fixé
    automatiquement pour les tâches créées dans l’espace
    utilisateur. Pour celles créées dans le noyau, ceci permet
    d’indiquer à Xenomai s’il faut sauvegarder ou non les
    registres de la FPU lors des commutations vers cette tâche.
T_JOINABLE :
    Cet attribut indique qu’il est possible d’attendre la fin
    de l’exécution de la tâche. Nous allons l’utiliser ci-après.
T_LOCK :
    Avant d’activer cette tâche, l’ordonnanceur sera verrouillé
    (aucune autre tâche ne pourra s’exécuter sur ce CPU)
    et il faudra le déverrouiller explicitement en utilisant
    rt_task_set_mode().
T_WARNSW :
    Si le thread passe du mode primaire au mode secondaire,
    il recevra un signal SIGDEBUG. Par défaut, ceci tue le
    processus, mais on peut le capturer pour analyser le
    problème. Nous le détaillerons ci-après.
```

### Exemple

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <alchemy/task.h>

void say_hello_world (void * unused)
{
    while (1) {
        rt_printf("Hello from Xenomai Realtime Space\n");
        rt_task_sleep(1000000000LL); // 1 milliard ns = 1 s.
    }
}

int main(void)
{
    int err;
    RT_TASK task;

    mlockall(MCL_CURRENT|MCL_FUTURE);

    if ((err = rt_task_spawn(& task, "Hello_01", 0, 50, T_JOINABLE, say_hello_world, NULL)) != 0) {
        fprintf(stderr, "rt_task_spawn: %s\n", strerror(-err));
        exit(EXIT_FAILURE);
    }
    rt_task_join(& task);
    return 0;
}
```

----

## Erreurs

Les fonctions de l’API Xenomai renvoient généralement zéro si tout s’est bien passé et une valeur d’erreur en cas d’échec.d’échec. Elles ne remplissent pas la variable globale
errno comme peuvent le faire d’autres fonctions de la bibliothèque C, aussi ne doiton pas appeler directement **perror()**, mais enregistrer le code d’erreur négatif et le transmettre à **strerror()** avant de l’afficher.

----

## Compilation

La compilation d’un programme avec l’API native de Xenomai va faire appel à un fichier Makefile dans lequel on invoquera le script xeno-config (installé dans /usr/xenomai/bin/ ) pour obtenir les paramètres de compilation à transmettre au compilateur gcc.

### Exemple de Makefile

```shell
APPLICATIONS = exemple-hello-01

SKIN = alchemy

XENOCONFIG=/usr/xenomai/bin/xeno-config
CC=$(shell $(XENOCONFIG) --cc)
CFLAGS=$(shell $(XENOCONFIG) --skin=$(SKIN) --cflags)
LDFLAGS=$(shell $(XENOCONFIG) --skin=$(SKIN) --ldflags)
LIBDIR=$(shell $(XENOCONFIG) --skin=$(SKIN) --libdir)

all:: $(APPLICATIONS)

clean:: $(RM) -f $(APPLICATIONS) *.o *~
```

Ensuite, il suffit de compiler et d'éxecuter :

```shell
make
./fichier_compile
```

----

## Thread

### Check du fonctionnement des threads Xenomai

Tout naturellement, il est nécessaire d’avoir les privilèges root pour exécuter une application avec des threads de hautes priorités.
Pendant le déroulement du thread, il est possible de l’observer ainsi :

```shell
cat /proc/xenomai/sched/threads
cat /proc/xenomai/shed/stat
```

*Explications du résultat*
Nous observons la présence du noyau Linux (ROOT) sur quatre CPU et du handler de l’interruption timer pour Xenomai. Vous pourrez observer combien de fois notre tâche a été activée (colonne Context Switches CSW) et il y a eu combien de commutations du mode primaire vers le secondaire (colonne Mode Switches MSW).

### Passer un thread Utilisateur en Temps-réel

Il est possible de basculer un thread déjà existant dans le domaine Linux en un thread temps réel sans en créer de nouveau. Pour cela, on invoque la fonction :*/

```c
int rt_task_shadow (RT_TASK * task,s
    const char * name,
    int priority,
    int mode);
```

Les seuls modes autorisés sont T_LOCK et T_WARNSW.
En voici un exemple très simple :

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <alchemy/task.h>

int main(void)
{
    int err;

    mlockall(MCL_CURRENT|MCL_FUTURE);

    if ((err = rt_task_shadow(NULL, "Hello World 02", 50, 0)) != 0) {
        fprintf(stderr, "rt_task_shadow: %s\n", strerror(-err));
        exit(EXIT_FAILURE);
    }
    while (1) {
        rt_printf("Hello World 02\n");
        rt_task_sleep(1000000000LL); // 1 sec.
    }
    return 0;
}
```

----

## Tâches périodiques

Deux méthodes pour réveiller une tâche périodique complémentaires : les réveils périodiques et les alarmes. Chaque tâche peut être régulièrement activée par un réveil personnel. Elle peut aussi recevoir les signaux d’une ou plusieurs alarmes (qui peuvent elles-mêmes être partagées entre plusieurs tâches). Le principe est simple : nous pouvons rendre un thread périodique, après sa création, grâce à la fonction :

```c
int rt_task_set_periodic (RT_TASK * tache, RTIME start, RTIME period);
```

### RT_TASK tache

Le premier paramètre est l’identifiant de la tâche. Dans le cas où une tâche veut se rendre périodique elle-même, elle peut employer NULL ou bien rt_task_self() qui lui renvoie toujours son propre identifiant :

```c
RT_TASK * rt_task_self (void);
```

### Unités de temps

Les deux autres paramètres s’expriment en unités d’horloge Xenomai. On peut configurer cette valeur, qui correspond également à la résolution de tous les paramètres temporels de l’API Alchemy, en ajoutant l’option "–alchemy-clock-resolution=n" sur la ligne de commande du processus :

- Item 1 si la valeur est zéro (ou si elle n’est pas indiquée sur la ligne de commande), les heures de déclenchement et les durées sont mesurées en nanosecondes ;
- Item 2 si la valeur n’est pas nulle, elle correspond à la durée (en microsecondes) d’un tick, et toutes les autres valeurs de temps sont exprimées en multiples de ce 11 tick.

Dans la plupart des cas, on préfère laisser la période de base de Xenomai à zéro, et exprimer les valeurs temporelles en nanosecondes. C’est ce que nous utiliserons dans la suite.

### RTIME start

Si la valeur est TM_NOW, l’activation est immédiate. On peut également utiliser la valeur rt_timer_read() qui nous renvoie la valeur du timer système, à laquelle on ajoute la durée désirée avant le premier déclenchement.

```c
RTIME rt_timer_read (void);
```

### RTIME period

Le troisième paramètre est la période de réveil du thread. La valeur TM_INFINITE permet d’arrêter l’activation régulière de la tâche. Comme Xenomai anticipe les déclenchements des timers pour corriger leur latence, il est indispensable que la valeur soit supérieure à celle se trouvant dans /proc/xenomai/latency (10 156 nanosecondes, soient 10,156 microsecondes sur mon Raspberry Pi 3 ).
Pour être réveillée, celle-ci doit préalablement être en attente sur l’appel rt_task_wait_period() :

```c
int rt_task_wait_period (unsigned long * overruns);
```

Cette fonction bloque la tâche appelante (sauf si la période n’a pas encore été programmée) jusqu’à la prochaine activation. Elle renvoie normalement zéro. En cas de problème (période non programmée, signal reçu, etc.), rt_task_wait_period() renvoie un code d’erreur négatif.
Le comportement global de la tâche périodique ressemblera donc à :

```c
while (rt_task_wait_period (NULL) == 0) {
    // traitement
}
```

Si le paramètre de rt_task_wait_period() n’est pas NULL, le pointeur sera renseigné avec le nombre d’activations manquées (dans le cas d'interruption par des tâches à priorité plus importantes).
Attention, si aucun dépassement n’a lieu, la valeur n’est pas modifiée, il est donc important d’initialiser la variable à zéro avant d’appeler la fonction.

### Exemple 

Mesure de temps entre activation des tâches périodiques (exemple-periodique.c) :

```c
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>

#include <alchemy/task.h>
#include <alchemy/timer.h>

void periodic_function (void * arg)
{
    RTIME previous = 0;
    RTIME now;
    RTIME period;
    RTIME duration;

    period = * (RTIME *) arg;
    period = period * 1000; // en ns
    rt_task_set_periodic(NULL, TM_NOW, period);

    while (1) {
        rt_task_wait_period(NULL);
        now = rt_timer_read();
        if (previous != 0) {
            duration = now - previous;
            rt_printf("%llu\n", duration/1000);
        }
        previous = now;
    }
}

int main(int argc, char * argv[])
{
    RT_TASK task;
    RTIME period;
    int err;

    mlockall(MCL_CURRENT|MCL_FUTURE);


    if ((argc != 2) || (sscanf(argv[1], "%llu", & period) != 1)) {
        fprintf(stderr, "usage: %s period_us\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if ((err = rt_task_spawn(& task, NULL, 0, 50, T_JOINABLE, periodic_function, &period)) != 0) {
        fprintf(stderr, "rt_task_spaw: %s\n", strerror(-err));
        exit(EXIT_FAILURE);
    }

    rt_task_join(& task);
    return 0;
}
```

----

## Dohell

Voir TP2 pour utilisation du script dohell (perturbation du processeur par des tâches TR).

----

## Sémaphore

(Voir cours pour explications)

### Fonctions usuelles

Les fonctions essentielles pour manipuler les sémaphores de Xenomai sont les suivantes :

```c
int rt_sem_create (RT_SEM * semaphore, const char * name, unsigned long count, int mode);
int rt_sem_delete (RT_SEM * semaphore);
int rt_sem_p (RT_SEM * semaphore, RTIME timeout);
int rt_sem_v (RT_SEM * semaphore);
```

### Explication des paramètres

Le dernier paramètre mode du rt_sem_create() peut prendre l’une des trois valeurs suivantes (tableau 5), ce qui représente une extension par rapport aux sémaphores usuels.

```c
S_PRIO :
    Lors d’une opération V(), le compteur est incrémenté et on réveille la
    tâche la plus prioritaire endormie dans une opération P() pour qu’elle
    puisse tester et décrémenter le compteur.
S_FIFO :
    Lors d’une opération V(), le compteur est incrémenté, puis on réveille
    la première tâche endormie, quelle que soit sa priorité.
S_PULSE :
    Lors d’une opération V(), on réveille la tâche la plus prioritaire. Si
    aucune tâche n’est en attente, le compteur n’est pas augmenté. Ainsi,
    le sémaphore joue un rôle de point de blocage pour chaque P() et de
    libération à chaque V(), ce qui rappelle le principe des variables
    conditions. Dans ce mode, le compteur doit obligatoirement être
    initialisé avec une valeur nulle (et il la conserve).
```

### Exemple (exemple-semaphore.c)

```c
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>

#include <alchemy/sem.h>

#include <alchemy/task.h>
#include <alchemy/timer.h>

#define NB_TASKS 6

static RT_SEM sem;
static int with_semaphore[NB_TASKS];

void thread_function (void * arg)
{
    int i;
    int num = (int) arg;

    for (i = 0; i < 4; i ++) {
        with_semaphore[num] = 0;
        rt_sem_p(& sem, TM_INFINITE);
        with_semaphore[num] = 1;
        sleep(2);
        rt_sem_v(& sem);
    }
    with_semaphore[num] = -1;
}


void observer(void * unused)
{
    int i;
    rt_printf("With semaphore    Without semaphore\n");
    while (1) {
        for (i = 0; i < NB_TASKS; i ++)
            if (with_semaphore[i] == 1) rt_printf("%d ", i);
        rt_printf ("          ");
        for (i = 0; i < NB_TASKS; i ++)
            if (with_semaphore[i] == 0) rt_printf("%d ", i);
        rt_printf("\n");
        rt_task_sleep(1000000000);
    }
}

int main(int argc, char * argv[])
{
    int i;
    int err;
    RT_TASK task[NB_TASKS];
    RT_TASK observ;

    mlockall(MCL_CURRENT|MCL_FUTURE);

    err = rt_sem_create(& sem, "Sem-01",
    NB_TASKS-2, S_PRIO);
    if (err != 0) {
    }        fprintf(stderr, "rt_sem_create:%s\n",
        strerror(-err));
        exit(EXIT_FAILURE);


    for (i = 0; i < NB_TASKS; i ++) {
        err = rt_task_spawn(& task[i], NULL,
        0, 99, T_JOINABLE,
        thread_function, (void *) i);
        if (err != 0) {
            fprintf(stderr, "rt_task_spawn:%s\n",
            strerror(-err));
            exit(EXIT_FAILURE);
        }
    }

    err = rt_task_spawn(& observ, NULL, 0, 99, T_JOINABLE, observer, NULL);
    if (err != 0) {
        fprintf(stderr, "rt_task_spawn:%s\n",
        strerror(-err));
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < NB_TASKS; i ++) rt_task_join(& task[i]);

    rt_task_delete(& observ);
    rt_sem_delete(& sem);

    return EXIT_SUCCESS;
}
```

*Précisions sur le code précédent*
Ce programme présente un défaut important qui est l’accès aux cases du tableau with_semaphore[] qui n’est pas protégé. Il y a donc un risque que la consultation faite par le thread observateur se produise au moment exact d’une écriture par l’un des autres threads et obtienne une valeur incohérente. Dans une application réelle, il conviendrait d’utiliser un autre mécanisme de verrouillage pour protéger l’accès au tableau, comme un mutex que nous verrons plus loin.*/

----

## Mutex

(Voir cours pour explications)

### Fonctions usuelles

```c
int rt_mutex_create (RT_MUTEX * mutex, const char * name);
int rt_mutex_delete (RT_MUTEX * mutex);
int rt_mutex_acquire (RT_MUTEX * mutex, RTIME timeout);
int rt_mutex_release (RT_MUTEX * mutex);
```

### Exemple d'utilisation

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>

#include <alchemy/task.h>
#include <alchemy/mutex.h>
#include <alchemy/timer.h>

static RT_MUTEX mutex;

void thread_1(void *unused);
void thread_2(void * unused);
void thread_3(void * unused);

void thread_1(void *unused)
{
    int err;
    RT_TASK task_2, task_3;
    rt_fprintf(stderr, "T1 starts\n");

    rt_fprintf(stderr, "T1 waits for the mutex\n");
    rt_mutex_acquire(& mutex, TM_INFINITE);

    rt_fprintf(stderr, "T1 holds the mutex\n");

    rt_fprintf(stderr, "T1 starts T3\n");
    if ((err = rt_task_spawn(& task_3, NULL, 0, 30, T_JOINABLE, thread_3, NULL) != 0)) {
        fprintf(stderr, "rt_task_spawn:%s\n",
        strerror(-err));
        exit(EXIT_FAILURE);
    }

    rt_fprintf(stderr, "T1 starts T2\n");
    if ((err = rt_task_spawn(& task_2, NULL, 0, 20,
        T_JOINABLE, thread_2, NULL) != 0)) {
        fprintf(stderr, "rt_task_spawn:%s\n",
        strerror(-err));
        exit(EXIT_FAILURE);
    }

    rt_fprintf(stderr, "T1 releases the mutex\n");
    rt_mutex_release(& mutex);

    rt_fprintf(stderr, "T1 waits for T2 & T3\n");
    rt_task_join(& task_2);
    rt_task_join(& task_3);

    rt_fprintf(stderr, "T1 quits\n");
}

void thread_2(void * unused)
{
    rt_fprintf(stderr, "    T2 starts\n");
    rt_fprintf(stderr, "    T2 works\n");
    rt_timer_spin(1500000000LLU);
    rt_fprintf(stderr, "    T2 quits\n");
}

void thread_3(void * unused)
{
    rt_fprintf(stderr, "        T3 starts\n");
    rt_fprintf(stderr, "        T3 waits for the mutex\n");
    rt_mutex_acquire(& mutex, TM_INFINITE);
    rt_fprintf(stderr, "        T3 holds the mutex\n");

    rt_fprintf(stderr, "        T3 works\n");
    rt_timer_spin(1500000000LLU);

    rt_fprintf(stderr, "        T3 releases the mutex\n");
    rt_mutex_release(& mutex);
    rt_fprintf(stderr, "        T3 quits\n");
}

int main(int argc, char * argv [])
{
    int err;
    RT_TASK task;

    mlockall(MCL_CURRENT|MCL_FUTURE);

    if ((err = rt_mutex_create(& mutex, "Mutex-exemple")) != 0) {
        fprintf(stderr, "rt_mutex_create:%s\n",
        strerror(-err));
        exit(EXIT_FAILURE);
    }

    if ((err = rt_task_spawn(& task, NULL, 0, 10, T_JOINABLE, thread_1, NULL) != 0)) {
        fprintf(stderr, "rt_task_spawn:%s\n",
        strerror(-err));
        exit(EXIT_FAILURE);
    }

    rt_task_join(& task);
    rt_mutex_delete(& mutex);
    return 0;
}
```

----

## Pipes

### Introduction

Dans Xenomai, il y existe des objets appelés RT_PIPE qui permettent à deux applications Xenomai et Linux de dialoguer à travers un pipe.
La règle pour ces tubes est la suivante :

- Item 1 une tâche Xenomai en mode primaire doit accéder à ce pipe sans basculer en mode secondaire, cette ressource doit donc être managée par Xenomai ;
- Item 2 un programme Linux doit également y accéder sans même savoir qu’il y a une tâche Xenomai à l’autre bout du pipe ; qui doit donc être managé par Linux. La solution l’est tout autant : le pipe a deux noms, un pour les applications Linux, un autre pour les Xenomai. Un RT_PIPE a deux noms :
  - Item 1 /dev/rtpN : pour les applications Linux ;
  - Item 2 rtpN : pour les applications Xenomai.

### Fonctions usuelles

```c
int rt_pipe_create (RT_PIPE *pipe, const char *name, int minor, size_t poolsize);
// Create a message pipe.
int rt_pipe_delete (RT_PIPE *pipe);
// Delete a message pipe.
ssize_t rt_pipe_receive (RT_PIPE *pipe, RT_PIPE_MSG **msgp, RTIME timeout);
// Receive a message from a pipe.
ssize_t rt_pipe_read (RT_PIPE *pipe, void *buf, size_t size, RTIME timeout);
// Read a message from a pipe.
ssize_t rt_pipe_send (RT_PIPE *pipe, RT_PIPE_MSG *msg, size_t size, int mode);
// Send a message through a pipe
ssize_t rt_pipe_write (RT_PIPE *pipe, const void *buf, size_t size, int mode);
// Write a message to a pipe.
ssize_t rt_pipe_stream (RT_PIPE *pipe, const void *buf, size_t size);
// Stream bytes to a pipe.
RT_PIPE_MSG * rt_pipe_alloc (RT_PIPE *pipe, size_t size);
// Allocate a message pipe buffer.
int rt_pipe_free (RT_PIPE *pipe, RT_PIPE_MSG *msg);
// Free a message pipe buffer.
int rt_pipe_flush (RT_PIPE *pipe, int mode);
// Flush the i/o queues associated with the kernel endpoint of a message pipe.
int rt_pipe_monitor (RT_PIPE *pipe, int(*fn)(RT_PIPE *pipe, int event, long arg));
// Monitor a message pipe asynchronously.
```

### Exemple Pipe côté Linux

```c
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
int main(void)
{
    char buf[128];
    int fd = open( "/dev/rtp0", O_RDWR );

    if (fd < 0)
    {
        printf("open error (%d)\n",fd);
        return 1;
    }

    if ( 0 < read(fd, buf, sizeof(buf)) )
    {
        printf("==> %s\n",buf);
    }
}
```

### Exemple Pipe côté Xenomai

```c
#include <stdio.h>
#include <sys/mman.h>
#include <alchemy/task.h>
#include <alchemy/pipe.h>

static RT_PIPE my_pipe;

static void task(void *arg)
{
    char *s="Hello world!";
    unsigned int sz=strlen(s)+1;
    if ( rt_pipe_write( &my_pipe, s, sz, P_NORMAL) != sz )
    {
        printf("rt_pipe_write error\n");
    }
}

int main (void)
{
    RT_TASK task_desc;

    /* disable memory swap */
    if ( mlockall( MCL_CURRENT | MCL_FUTURE ) != 0 )
    {
        printf("mlockall error\n");
        return 1;
    }

    if ( rt_pipe_create( &my_pipe, "rtp0", P_MINOR_AUTO, 0 ) != 0 )
    {
        printf("rt_pipe_create error\n");
        return 1;
    }

    if (rt_task_spawn( &task_desc, /* task descriptor */
        "my task", /* name */
        0 /* 0 = default stack size */,
        99 /* priority */,
        T_JOINABLE, /* needed to call rt_task_join after */
        &task, /* entry point (function pointer) */
        NULL /* function argument */ )!=0)
    {
        printf("rt_task_spawn error\n");
        return 1;
    }

    /* wait for task function termination */
    rt_task_join(&task_desc);
    return 0;
}
```

## Annexes

### Xenomai

[Gitlab Xenomai](https://gitlab.denx.de/Xenomai/xenomai/-/tree/master/include%2Falchemy)

### Make

Lien vers le [Manuel](https://www.gnu.org/software/make/manual/make.html)
